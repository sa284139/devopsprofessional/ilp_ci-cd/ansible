### ansible

```
ILP CI-CD project - ansible
```
---

### Run Ansible Playbook

```
ansible-playbook server.yaml
```
---

### Roles

1. java
```
Ansible role to check JAVA environment and setup JAVA environment if not exists
```

2. tomcat
```
Ansible role to download and setup tomcat container
```

3. deploy
```
Ansible role to download artifact from JFrog Artifactory and deploy to tomcat container
```
---
